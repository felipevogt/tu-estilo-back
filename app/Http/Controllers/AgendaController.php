<?php

namespace App\Http\Controllers;

use App\Mail\NotificarReserva;
use App\Models\Agendar_Hora;
use App\Models\Cliente;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use stdClass;

class AgendaController extends Controller
{
    public function obtenerHorasDisponibles(Request $request)
    {

        $fecha_busqueda = $request->fecha;


        $servicios = $request->servicios;

        if (count($servicios) == 0) {
            return null;
        }

        $servicio = null;
        $tiempo_servicio = 0;
        foreach ($servicios as $key => $s) {
            $servicio = Servicio::find($s);
            $tiempo_servicio = $tiempo_servicio + $servicio->tiempo;
        }

        if ($servicio == null && $tiempo_servicio == 0) {
            return null;
        }

        $establecimiento = $servicio->establecimiento;

        $hora_inicio = strtotime($establecimiento->hora_inicio);
        $hora_fin = strtotime($establecimiento->hora_fin);
        $establecimiento_capacidad = $establecimiento->capacidad;
        $establecimiento_hora_fin = $this->hoursToMinutes($establecimiento->hora_fin);
        $horas = array();

        while ($hora_inicio < $hora_fin) {

            $hora_pivot =  $hora_inicio + ($tiempo_servicio * 60);

            $hora = new stdClass();
            $hora->hora_inicio = date('H', $hora_inicio) . ':' . date('i', $hora_inicio);
            $hora->hora_fin =  date('H', $hora_pivot) . ':' . date('i', $hora_pivot);

            $horas[] = $hora;

            $hora_inicio = $hora_pivot;
        }

        $horas_disponibles = array();

        $agenda_dia = DB::table('agendar_hora_servicio')
            ->join('agendar_hora', 'agendar_hora.id', '=', 'agendar_hora_servicio.agendar_hora_id')
            ->join('servicio', 'servicio.id', '=', 'agendar_hora_servicio.servicio_id')
            ->join('establecimiento', 'establecimiento.id', '=', 'servicio.establecimiento_id')
            ->select('agendar_hora.*')
            ->where('establecimiento.id', '=', $establecimiento->id)
            ->whereDate('agendar_hora.fecha', $fecha_busqueda)
            ->whereIn('agendar_hora.estado', [1, 2])
            ->get();

        foreach ($horas as $key => $h) {
            $minutos_inicio = $this->hoursToMinutes($h->hora_inicio) + 1;
            $minutos_fin = $this->hoursToMinutes($h->hora_fin) - 1;

            $contador = 0;
            foreach ($agenda_dia as $key => $reserva) {
                if (intval($reserva->hora_inicio) <= $minutos_inicio && intval($reserva->hora_fin) >= $minutos_inicio || intval($reserva->hora_inicio) <= $minutos_fin && intval($reserva->hora_fin) >= $minutos_fin) {
                    $contador = $contador + 1;
                }
            }

            if ($contador < $establecimiento_capacidad) {
                if ($minutos_fin <= $establecimiento_hora_fin) {
                    $horas_disponibles[] = $h;
                }
            }
        }



        return response()->json($horas_disponibles);
    }

    public function reservarHora(Request $request)
    {
        $cliente = Cliente::where('user_id', $request->usuario)->first();

        if (!$cliente) {
            $status = new stdClass();
            $status->estado = 404;
            $status->mensaje = 'No se encuentra el usuario';

            return response()->json($status);
        }

        $servicios = $request->servicios;

        if (count($servicios) == 0) {
            return null;
        }

        $agenda_existente = Agendar_Hora::whereHas('servicio', function ($query) use ($servicios) {
            $query->whereIn('servicio_id', $servicios);
        })
            ->where('cliente_id', $cliente->id)
            ->whereDate('fecha', $request->fecha)
            ->where('estado', 1)
            ->first();

        if ($agenda_existente) {
            $status = new stdClass();
            $status->estado = 401;
            $status->mensaje = 'Existe una reserva pendiente de este servicio para el dia ' . $request->fecha;

            return response()->json($status);
        }

        $agenda = Agendar_Hora::create([
            'hora_inicio' => $this->hoursToMinutes($request->hora_inicio),
            'hora_fin' => $this->hoursToMinutes($request->hora_fin),
            'estado' => 1,
            'cliente_id' => $cliente->id,
            'fecha' => $request->fecha,
        ]);

        foreach ($servicios as $key => $s) {
            $agenda->servicio()->attach($s);
        }

        if ($agenda) {
            $status = new stdClass();
            $status->estado = 201;
            $status->mensaje = 'Reserva realizada';

            return response()->json($status);
        } else {
            $status = new stdClass();
            $status->estado = 402;
            $status->mensaje = 'Ha surgido un error';

            return response()->json($status);
        }

        return response()->json($agenda);
    }

    public function getReservas($id)
    {

        $agendaServicios = DB::table('agendar_hora_servicio')
            ->leftJoin('servicio', 'servicio.id', '=', 'agendar_hora_servicio.servicio_id')
            ->select(
                'agendar_hora_servicio.agendar_hora_id',
                DB::raw('group_concat(distinct servicio.nombre separator ", ") AS servicio_nombre'),
            )
            ->where('servicio.establecimiento_id', '=', $id)
            ->groupBy('agendar_hora_servicio.agendar_hora_id');

        $reservas = DB::table('agendar_hora')
            ->joinSub($agendaServicios, 'agenda_servicios', function ($join) {
                $join->on('agendar_hora.id', '=', 'agenda_servicios.agendar_hora_id');
            })
            ->leftJoin('cliente', 'cliente.id', '=', 'agendar_hora.cliente_id')
            ->select('agendar_hora.*', 'agenda_servicios.*', 'cliente.nombre', 'cliente.apellido_paterno', 'cliente.telefono')
            ->where('agendar_hora.estado', '=', 1)
            ->get();


        return response()->json($reservas);
    }

    public function getReservasConfirmadas($id)
    {
        $agendaServicios = DB::table('agendar_hora_servicio')
            ->leftJoin('servicio', 'servicio.id', '=', 'agendar_hora_servicio.servicio_id')
            ->select(
                'agendar_hora_servicio.agendar_hora_id',
                DB::raw('group_concat(distinct servicio.nombre separator ", ") AS servicio_nombre'),
            )
            ->where('servicio.establecimiento_id', '=', $id)
            ->groupBy('agendar_hora_servicio.agendar_hora_id');

        $reservas = DB::table('agendar_hora')
            ->joinSub($agendaServicios, 'agenda_servicios', function ($join) {
                $join->on('agendar_hora.id', '=', 'agenda_servicios.agendar_hora_id');
            })
            ->leftJoin('cliente', 'cliente.id', '=', 'agendar_hora.cliente_id')
            ->select('agendar_hora.*', 'agenda_servicios.*', 'cliente.nombre', 'cliente.apellido_paterno', 'cliente.telefono')
            ->where('agendar_hora.estado', '=', 2)
            ->get();


        return response()->json($reservas);
    }

    public function getReservasCliente($id)
    {
        $cliente = Cliente::where('user_id', $id)->first();

        if (!$cliente) {
            $status = new stdClass();
            $status->estado = 404;
            $status->mensaje = 'No se encuentra el usuario';

            return response()->json($status);
        }

        $agendaServicios = DB::table('agendar_hora_servicio')
            ->leftJoin('servicio', 'servicio.id', '=', 'agendar_hora_servicio.servicio_id')
            ->leftJoin('establecimiento', 'establecimiento.id', '=', 'servicio.establecimiento_id')
            ->select(
                'agendar_hora_servicio.agendar_hora_id',
                'establecimiento.nombre AS establecimiento_nombre',
                DB::raw('group_concat(distinct servicio.nombre separator ", ") AS servicio_nombre'),
            )
            ->groupBy('agendar_hora_servicio.agendar_hora_id', 'establecimiento_nombre');

        $reservas = DB::table('agendar_hora')
            ->joinSub($agendaServicios, 'agenda_servicios', function ($join) {
                $join->on('agendar_hora.id', '=', 'agenda_servicios.agendar_hora_id');
            })
            ->leftJoin('cliente', 'cliente.id', '=', 'agendar_hora.cliente_id')
            ->select('agendar_hora.*', 'agenda_servicios.*')
            ->where('agendar_hora.cliente_id', '=', $cliente->id)
            ->get();

        $horas_calendario = array();

        foreach ($reservas as $key => $reserva) {
            $calendario = new stdClass();
            $calendario->name = $reserva->establecimiento_nombre;
            $calendario->start = $this->formatDate($reserva->fecha) . " " . $this->convertToHoursMins($reserva->hora_inicio);
            $calendario->end = $this->formatDate($reserva->fecha) . " " . $this->convertToHoursMins($reserva->hora_fin);
            $calendario->color = $this->getColor($reserva->estado);
            $calendario->estado = $this->getEstado($reserva->estado);
            $calendario->servicios = $reserva->servicio_nombre;
            $horas_calendario[] = $calendario;
        }

        return response()->json($horas_calendario);
    }

    public function confirmarReserva(Request $request, $id)
    {
        $reserva = Agendar_Hora::find($id);

        $user = $reserva->cliente->user;
        $status = new stdClass();

        if ($request->confirmacion == true) {
            $reserva->update([
                'estado' => 2,
            ]);
            $status->estado = 201;
            $status->mensaje = 'Reserva aprobada con exito';
        } else {
            $reserva->update([
                'estado' => 0,
            ]);
            $status->estado = 202;
            $status->mensaje = 'Reserva rechazada con exito';
        }


        Mail::to($user->email)->send(new NotificarReserva($reserva));

        return response()->json($status);
    }

    function hoursToMinutes($hours)
    {
        $minutes = 0;
        if (strpos($hours, ':') !== false) {
            // Split hours and minutes. 
            list($hours, $minutes) = explode(':', $hours);
        }
        return $hours * 60 + $minutes;
    }

    function convertToHoursMins($time, $format = '%02d:%02d')
    {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    function formatDate($date)
    {
        return date("Y-m-d", strtotime($date));
    }

    function getColor($estado)
    {
        if ($estado == 0) {
            return 'red';
        } else if ($estado == 2) {
            return 'green';
        } else {
            return 'orange';
        }
    }

    function getEstado($estado)
    {
        if ($estado == 0) {
            return 'Rechazada';
        } else if ($estado == 2) {
            return 'Aprobada';
        } else {
            return 'Pendiente';
        }
    }
}
