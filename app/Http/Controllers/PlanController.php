<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function getPlanes()
    {
        $planes = Plan::orderBy('id', 'asc')->get();
        return response()->json($planes);
    }
}
