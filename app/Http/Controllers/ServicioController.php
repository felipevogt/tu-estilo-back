<?php

namespace App\Http\Controllers;

use App\Models\Servicio;
use Illuminate\Http\Request;
use App\Models\FileUpload;
use Error;
use Symfony\Component\CssSelector\Exception\SyntaxErrorException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ServicioController extends Controller
{
    public function getServicios($id_establecimiento)
    {
        $servicios = Servicio::where('establecimiento_id', $id_establecimiento)->with('personal')->get();
        return response()->json($servicios);
    }

    public function store(Request $request, $id_establecimiento)
    {
        try {
            $file_path = $this->uploadImg($request);

            $servicio = Servicio::create([
                'nombre' => $request->nombre,
                'precio' => $request->precio,
                'tiempo' => $request->tiempo,
                'img' => $file_path,
                'descripcion' => $request->descripcion,
                'personal_id' => $request->personal_id,
                'establecimiento_id' => $id_establecimiento,
            ]);
            $servicio->personal;

            return response()->json($servicio);
        } catch (\Throwable $th) {
            throw new HttpException(500, $th->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $servicio = Servicio::find($id);

        if ($request->editImg == 'false') {
            $file_path = $this->uploadImg($request);
            $servicio->update([
                'img' => $file_path,
            ]);
        }

        $servicio->update([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'tiempo' => $request->tiempo,
            'descripcion' => $request->descripcion,
            'personal_id' => $request->personal_id,
        ]);

        $servicio->personal;
        return response()->json($servicio);
    }

    public function delete($id)
    {
        $servicio = Servicio::find($id);

        $servicio->delete();
        return response(true);
    }

    public function uploadImg(Request $request)
    {
        $file_path = '';
        if ($request->file()) {
            $file_name = time() . '-' . $request->file('img')->getClientOriginalName();;
            $file_path = $request->file('img')->storeAs('images/servicios', $file_name, 'public');
            return $file_path;
        } else {
            throw new SyntaxErrorException("No existe una imagen.");
        }
    }
}
