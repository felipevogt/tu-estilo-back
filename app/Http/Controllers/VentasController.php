<?php

namespace App\Http\Controllers;

use App\Models\Ventas;
use Illuminate\Http\Request;

class VentasController extends Controller
{
    public function getVentas($id_establecimiento)
    {
        $ventas = Ventas::where('establecimiento_id', $id_establecimiento)->with('servicios')->get();
        return response()->json($ventas);
    }

    public function store(Request $request, $id_establecimiento)
    {
        $venta = Ventas::create([
            'total' => $request['total'],
            'servicio_id' => $request['servicio_id'],
            'metodo_pago' => $request['metodo_pago'],
            'rut_vendedor' => $request['rut_vendedor'],
            'fecha_venta' => $request['fecha_venta'],
            'establecimiento_id' => $id_establecimiento,
        ]);

        foreach ($request['servicios'] as $servicio) {
            $venta->servicios()->attach($servicio['id']);
        }

        $venta->servicios;

        return response()->json($venta);
    }

    public function update(Request $request, $id)
    {
        $venta = Ventas::find($id);

        $venta->update([
            'total' => $request['total'],
            'servicio_id' => $request['servicio_id'],
            'metodo_pago' => $request['metodo_pago'],
            'rut_vendedor' => $request['rut_vendedor'],
            'fecha_venta' => $request['fecha_venta'],
        ]);

        $venta->servicios()->detach();
        foreach ($request['servicios'] as $servicio) {
            $venta->servicios()->attach($servicio['id']);
        }

        $venta->servicios;
        return response()->json($venta);
    }

    public function delete($id)
    {
        $material = Ventas::find($id);

        $material->delete();
        return response(true);
    }
}
