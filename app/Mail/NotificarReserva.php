<?php

namespace App\Mail;

use App\Models\Agendar_Hora;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarReserva extends Mailable
{
    use Queueable, SerializesModels;

    public $agenda;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Agendar_Hora $agenda)
    {
        $this->agenda = $agenda;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->agenda->estado == 2){
            return $this->subject('Reservación confirmada')->view('mails.notificar_reserva')->with([
                'fecha' =>  $this->formatDate($this->agenda->fecha),
                'titulo' => 'Reservación confirmada',
                'estado' => 'Confirmada',
                'hora_inicio' => $this->convertToHoursMins($this->agenda->hora_inicio),
                'hora_fin' => $this->convertToHoursMins($this->agenda->hora_fin),
            ]);
        }else if($this->agenda->estado == 0){
            return $this->subject('Reservación rechazada')->view('mails.notificar_reserva')->with([
                'fecha' => $this->formatDate($this->agenda->fecha),
                'titulo' => 'Reservación rechazada',
                'estado' => 'Rechazada',
                'hora_inicio' => $this->convertToHoursMins($this->agenda->hora_inicio),
                'hora_fin' => $this->convertToHoursMins($this->agenda->hora_fin),
            ]);
        }

    }

    function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    function formatDate($date){
        return date("d/m/Y", strtotime($date));
    }
}
