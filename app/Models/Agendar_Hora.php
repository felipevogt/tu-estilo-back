<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agendar_Hora extends Model
{
    use HasFactory;
    protected $table = 'agendar_hora';
    protected $primaryKey = 'id';
    protected $fillable = ['fecha', 'hora_inicio', 'hora_fin', 'estado', 'cliente_id'];
    public $timestamps = true;

    public function servicio()
    {
        return $this->belongsToMany(Servicio::class, 'agendar_hora_servicio', 'agendar_hora_id', 'servicio_id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
}
