<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    use HasFactory;
    protected $table = 'establecimiento';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre',
        'capacidad',
        'capacidad_diaria',
        'descripcion',
        'telefono',
        'hora_inicio',
        'hora_fin',
        'img',
        'tipo_establecimiento_id',
        'direccion_id',
        'propietario_id',
        'estado',
        'estado_premium',
        'duracion_plan',
        'duracion_plan_premium'
    ];

    public $timestamps = true;

    public function tipo_establecimiento()
    {
        return $this->belongsTo(Tipo_Establecimiento::class);
    }
    public function direccion()
    {
        return $this->belongsTo(Direccion::class);
    }
    public function propietario()
    {
        return $this->belongsTo(Propietario::class);
    }
    public function personales()
    {
        return $this->hasMany(Personal::class);
    }
    public function servicios()
    {
        return $this->hasMany(Servicio::class);
    }
    public function puntuaciones()
    {
        return $this->hasMany(Puntuacion::class);
    }
    public function materiales()
    {
        return $this->hasMany(Materiales::class);
    }
    public function ventas()
    {
        return $this->hasMany(Ventas::class);
    }
    public function solicitudes()
    {
        return $this->hasMany(Solicitud::class);
    }
}
