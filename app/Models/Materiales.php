<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materiales extends Model
{
    use HasFactory;
    protected $table = 'materiales';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'cantidad', 'categoria_material_id', 'establecimiento_id'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
    public function categoria_material()
    {
        return $this->belongsTo(Categoria_Material::class);
    }
}
