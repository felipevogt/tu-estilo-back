<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    use HasFactory;
    protected $table = 'personal';
    protected $primaryKey = 'id';
    protected $fillable = ['rut', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'especialidad', 'establecimiento_id', 'user_id'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function servicios()
    {
        return $this->hasMany(Servicio::class);
    }
}
