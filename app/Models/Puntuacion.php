<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puntuacion extends Model
{
    use HasFactory;
    protected $table = 'puntuacion';
    protected $primaryKey = 'id';
    protected $fillable = ['comentario', 'puntuacion', 'cliente_id', 'establecimiento_id'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
}
