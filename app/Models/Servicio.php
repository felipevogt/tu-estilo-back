<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    use HasFactory;
    protected $table = 'servicio';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'precio', 'tiempo', 'descripcion', 'img', 'establecimiento_id', 'personal_id'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
    public function ventas()
    {
        return $this->belongsToMany(Ventas::class);
    }
    public function horas_reservadas()
    {
        return $this->belongsToMany(Agendar_Hora::class, 'agendar_hora_servicio', 'servicio_id', 'agendar_hora_id');
    }
}
