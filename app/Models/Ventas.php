<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    use HasFactory;
    protected $table = 'ventas';
    protected $primaryKey = 'id';
    protected $fillable = ['total', 'metodo_pago', 'rut_vendedor', 'fecha_venta', 'establecimiento_id'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
    public function servicios()
    {
        return $this->belongsToMany(Servicio::class);
    }
}
