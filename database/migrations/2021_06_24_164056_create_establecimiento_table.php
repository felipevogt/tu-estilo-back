<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablecimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimiento', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('telefono');
            $table->string('img');
            $table->string('descripcion');
            $table->integer('capacidad');
            $table->integer('capacidad_diaria');
            $table->string('hora_inicio');
            $table->string('hora_fin');
            $table->boolean('estado')->default(0);
            $table->boolean('estado_premium')->default(0);
            $table->dateTime('duracion_plan')->nullable();
            $table->dateTime('duracion_plan_premium')->nullable();
            $table->foreignId('tipo_establecimiento_id')->constrained('tipo_establecimiento');
            $table->foreignId('direccion_id')->constrained('direccion');
            $table->bigInteger('propietario_id')->unsigned()->unique();
            $table->foreign('propietario_id')->references('id')->on('propietario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimiento');
    }
}
