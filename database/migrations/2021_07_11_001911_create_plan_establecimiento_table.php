<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanEstablecimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_plan', function (Blueprint $table) {
            $table->id();
            $table->boolean('estado')->default(0);
            $table->foreignId('plan_id')->constrained('plan');
            $table->foreignId('establecimiento_id')->constrained('establecimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_establecimiento');
    }
}
