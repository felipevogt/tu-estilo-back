<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgendarHoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendar_hora', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fecha')->nullable();
            $table->integer('hora_inicio')->nullable();
            $table->integer('hora_fin')->nullable();
            $table->integer('estado')->default(0);
            $table->foreignId('cliente_id')->constrained('cliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
