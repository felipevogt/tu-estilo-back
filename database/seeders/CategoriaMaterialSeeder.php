<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria_material')->insert([
            'categoria' => 'Limpieza',
        ]);
        DB::table('categoria_material')->insert([
            'categoria' => 'Tintura',
        ]);
        DB::table('categoria_material')->insert([
            'categoria' => 'Otros',
        ]);
        DB::table('categoria_material')->insert([
            'categoria' => 'Herramientas',
        ]);
    }
}
