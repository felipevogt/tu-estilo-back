<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insertGetId([
            'email' => 'superadmin@tuestilo.com',
            'password' => Hash::make('admin'),
            'type' => 04,
        ]);
    }
}
