<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plan')->insert([
            'nombre' => 'Plan 30 dias',
            'dias' => 30,
            'precio' => 3000,
        ]);
        DB::table('plan')->insert([
            'nombre' => 'Plan 90 dias',
            'dias' => 90,
            'precio' => 8000,
        ]);
        DB::table('plan')->insert([
            'nombre' => 'Plan 360 dias',
            'dias' => 360,
            'precio' => 30000,
        ]);
        DB::table('plan')->insert([
            'nombre' => 'Plan Premium',
            'dias' => 360,
            'precio' => 100000,
        ]);
    }
}
