<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PropietarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $idUser = DB::table('user')->insertGetId([
                'email' => 'emailpropietario' . $i . '@gmail.com',
                'password' => Hash::make('admin'),
                'type' => 01,
            ]);
            DB::table('propietario')->insert([
                'rut' => $i . '.123.123-0',
                'nombre' => 'Nombre Propietario' . $i,
                'apellido_paterno' => 'ApellidoP' . $i,
                'apellido_materno' => 'ApellidoM' . $i,
                'telefono' => '123123' . $i,
                'user_id' => $idUser,
            ]);
        }
    }
}
