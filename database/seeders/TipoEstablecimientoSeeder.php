<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoEstablecimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_establecimiento')->insert([
            'tipo' => 'Barberia',
        ]);
        DB::table('tipo_establecimiento')->insert([
            'tipo' => 'Peluqueria',
        ]);
    }
}
