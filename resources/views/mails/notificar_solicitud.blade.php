<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>{{ $titulo }}</title>
</head>

<body>
    <h2>{{ $titulo }}</h2>
    <br/>
    <p>Tu solicitud para el plan: <strong>{{ $nombre }}</strong>, que cuenta con una duración de <strong>{{ $duracion }} dias.</strong></p>
    <p>Ha sido <strong>{{ $estado_plan }}</strong>.</p>
    <br/>
    <br/>
    <p>En caso de consultas o reclamos, favor comunicarse al siguiente número: +56932976983</p>
    <p>--------------------------------------------------------------------------------------------</p>
    <p>Atentamente TuEstilo</p>
</body>

</html>